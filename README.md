Below is a problem that we need a solution for:

In a bag, there are n identical balls (n >= 1) that needs to be removed from the bag.<br/>
You can either remove one ball or two balls at a time. Write a code that, given n, calculates how many different ways you can remove the balls from the bag.

Example:<br/>
n = 3, result = 3<br/>
1 - 1 - 1,<br/>
1 - 2,<br/>
2 - 1<br/>

Example:<br/>
n = 5, result = 8<br/>
1 - 1 - 1 - 1 - 1<br/>
1 - 1 - 1 - 2<br/>
1 - 1 - 2 - 1<br/>
1 - 2 - 1 - 1<br/>
2 - 1 - 1 - 1<br/>
1 - 2 - 2<br/>
2 - 1 - 2<br/>
2 - 2 - 1<br/>
<br/>
ways(n) = ways(n-1) + ways(n-2)