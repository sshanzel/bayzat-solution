let total = 0;
const n = 5; // kindly change the value for the desired variation
const options = [1, 2];

function checkNode(currentValue) {
  if (currentValue == 0) total += 1;
  
  for (let i = 0; i < options.length; i++)
    if ((currentValue - options[i]) >= 0)
      checkNode(currentValue - options[i]);
}

checkNode(n);

console.log(total);